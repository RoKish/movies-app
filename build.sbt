name := """movies-app"""
organization := "net.rokish"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice

val hibernateVersion = "5.2.10.Final"

libraryDependencies ++= Seq(
  javaJdbc,
  "com.h2database" % "h2" % "1.4.196",
  javaJpa,
  "org.hibernate" % "hibernate-core" % hibernateVersion,
  "org.hibernate" % "hibernate-entitymanager" % hibernateVersion // replace by your jpa implementation
)

PlayKeys.externalizeResources := false

libraryDependencies += javaWs

libraryDependencies ++= Seq(
  "org.testng" % "testng" % "6.11" % "test",
  "org.mockito" % "mockito-core" % "2.8.47" % "test"
)

libraryDependencies += "io.swagger" % "swagger-play2_2.12" % "1.6.0"

