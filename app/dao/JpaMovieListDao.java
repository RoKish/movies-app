package dao;

import com.typesafe.config.Config;
import models.MovieList;
import org.hibernate.exception.ConstraintViolationException;
import play.db.jpa.JPAApi;
import play.libs.concurrent.HttpExecutionContext;
import util.ConfigUtil;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class JpaMovieListDao implements MovieListDao {

    private static final String PERSISTENCE_UNIT_NAME = "default";

    private final DatabaseExecutionContext databaseExecutionContext;
    private final HttpExecutionContext httpExecutionContext;
    private final JPAApi jpaApi;
    private final int pageSize;

    @Inject
    public JpaMovieListDao(DatabaseExecutionContext databaseExecutionContext, HttpExecutionContext httpExecutionContext, JPAApi jpaApi, Config config) {
        this.databaseExecutionContext = databaseExecutionContext;
        this.httpExecutionContext = httpExecutionContext;
        this.jpaApi = jpaApi;
        this.pageSize = ConfigUtil.getPageSize(config);
    }

    @Override
    public CompletionStage<MovieList> save(MovieList movieList) {
        return withTransaction(em -> {
            em.persist(movieList);
            // TODO: Phantom reads
//            Long count = em.createQuery("SELECT COUNT(l.name) FROM MovieList l WHERE l.name=:name AND l.active=true", Long.class)
//                    .setParameter("name", movieList.getName())
//                    .getSingleResult();
//            if (count > 1) {
//                throw new DataAccessException(DataAccessExceptionType.CONSTRAINT, "The movie list '"  + movieList.getName() + "' already exists");
//            }
            return movieList;
        }).exceptionally(exception -> {
            Throwable cause = unwrapCause(exception);
            if (cause instanceof ConstraintViolationException) {
                throw new DataAccessException(DataAccessExceptionType.ALREADY_EXISTS,
                        "The movie list with name '"  + movieList.getName() + "' already exists");
            } else {
                throw new DataAccessException(DataAccessExceptionType.INTERNAL, "An error occurred while saving the movie " +
                        "list in the database", exception);
            }
        });
    }

    private Throwable unwrapCause(Throwable throwable) {
        Throwable cause = throwable;
        if (cause instanceof CompletionException) {
            cause = cause.getCause();
        }
        if (cause instanceof RollbackException) {
            if (cause.getCause() instanceof PersistenceException) {
                cause = cause.getCause().getCause();
            } else {
                cause = cause.getCause();
            }
        }
        return cause;
    }

    @Override
    public CompletionStage<MovieList> get(int id) {
        return withTransaction(true, em -> em.find(MovieList.class, id));
    }

    @Override
    public CompletionStage<Stream<MovieList>> getAll(int page) {
        return withTransaction(em -> em.createNativeQuery("SELECT * FROM movie_list l WHERE active=true OFFSET :offset LIMIT :limit", MovieList.class)
                .setParameter("offset", (page - 1) * pageSize)
                .setParameter("limit", pageSize)
                .getResultList()
                .stream());
    }

    @Override
    public CompletionStage<MovieList> saveMovies(int id, Set<Integer> movies) {
        return withTransaction(em -> {
            MovieList movieList = findMovieListRequired(em, id);
            movieList.getMovies().addAll(movies);
            return movieList;
        });
    }

    @Override
    public CompletionStage<MovieList> delete(int id) {
        return withTransaction(em -> {
            MovieList movieList = em.find(MovieList.class, id);
            if (movieList != null) {
                em.remove(movieList);
            }
            return movieList;
        });
    }

    @Override
    public CompletionStage<MovieList> deleteMovie(int listId, int movieId) {
        return withTransaction(em -> {
            MovieList movieList = findMovieListRequired(em, listId);
            movieList.getMovies().remove(movieId);
            return movieList;
        });
    }

    private <T> CompletionStage<T> withTransaction(Function<EntityManager, T> block) {
        return withTransaction(false, block);
    }

    private <T> CompletionStage<T> withTransaction(boolean readOnly, Function<EntityManager, T> block) {
        return CompletableFuture.supplyAsync(() -> jpaApi.withTransaction(PERSISTENCE_UNIT_NAME, readOnly, block), databaseExecutionContext)
                .thenApplyAsync(Function.identity(), httpExecutionContext.current());
    }

    private MovieList findMovieListRequired(EntityManager entityManager, int id) {
        MovieList movieList = entityManager.find(MovieList.class, id);
        if (movieList == null) {
            throw new DataAccessException(DataAccessExceptionType.NOT_FOUND, "The movie list doesn't exist: " + id);
        }
        return movieList;
    }


}
