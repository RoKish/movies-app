package dao;

import akka.actor.ActorSystem;
import play.libs.concurrent.CustomExecutionContext;

import javax.inject.Inject;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class DatabaseExecutionContext extends CustomExecutionContext {

    public static final String NAME = "database.dispatcher";

    @Inject
    public DatabaseExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, NAME);
    }
}
