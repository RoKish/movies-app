package dao;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public enum DataAccessExceptionType {
    NOT_FOUND,
    ALREADY_EXISTS,
    INTERNAL
}
