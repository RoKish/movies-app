package dao;

import com.google.inject.ImplementedBy;
import models.MovieList;

import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@ImplementedBy(JpaMovieListDao.class)
public interface MovieListDao {

    CompletionStage<MovieList> save(MovieList movieList);

    CompletionStage<MovieList> get(int id);

    CompletionStage<Stream<MovieList>> getAll(int page);

    CompletionStage<MovieList> saveMovies(int id, Set<Integer> movies);

    CompletionStage<MovieList> delete(int id);

    CompletionStage<MovieList> deleteMovie(int listId, int movieId);

}
