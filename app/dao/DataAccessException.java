package dao;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class DataAccessException extends RuntimeException {

    private final DataAccessExceptionType type;

    public DataAccessException(DataAccessExceptionType type, String message) {
        super(message);
        this.type = type;
    }

    public DataAccessException(DataAccessExceptionType type, String message, Throwable cause) {
        super(message, cause);
        this.type = type;
    }

    public DataAccessExceptionType getType() {
        return type;
    }

}
