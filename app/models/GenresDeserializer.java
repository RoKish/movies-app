package models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class GenresDeserializer extends JsonDeserializer<Set<String>> {

    @Override
    public Set<String> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode json = p.getCodec().readTree(p);
        Set<String> genres = new HashSet<>();
        if (json.isArray()) {
            ArrayNode arrayNode = (ArrayNode) json;
            arrayNode.elements().forEachRemaining(genre -> {
                JsonNode name = genre.get("name");
                if (name != null) {
                    genres.add(name.asText());
                }
            });
        }

        return genres;
    }

}
