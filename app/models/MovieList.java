package models;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.Set;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@Entity
@Table(
        name = "movie_list",
        indexes = @Index(name = "movie_list_active_index", columnList = "active")
)
@Where(clause = "active = true")
@SQLDelete(sql = "UPDATE movie_list SET active=false WHERE movie_list_id = ?")
public class MovieList {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "movie_list_id")
    private int id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(columnDefinition = "boolean DEFAULT true")
    private boolean active;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "movie_reference",
            joinColumns = @JoinColumn(name = "movie_list_id"),
            foreignKey = @ForeignKey(name = "fk_movie_movie_list")
    )
    @Column(name = "movie_id", nullable = false)
    private Set<Integer> movies;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<Integer> getMovies() {
        return movies;
    }

    public void setMovies(Set<Integer> movies) {
        this.movies = movies;
    }
}
