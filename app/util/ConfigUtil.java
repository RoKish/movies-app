package util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class ConfigUtil {

    private static final String NOT_VALID_URL_ERROR_MESSAGE = "The value must represent a valid URL";

    private ConfigUtil() {
    }

    public static URL getUrl(Config config, String path) {
        try {
            return new URL(config.getString(path));
        } catch (Exception e) {
            throw new ConfigException.BadValue(path, NOT_VALID_URL_ERROR_MESSAGE, e);
        }
    }

    public static URL getNormalizedUrl(Config config, String path) {
        try {
            URL url = new URL(config.getString(path));
            int port = url.getPort() == 80 ? -1 : url.getPort();
            String file = url.getPath();
            if (file.endsWith("/")) {
                file = file.substring(0, file.length() - 1);
            }
            return new URL(url.getProtocol(), url.getHost(), port, file);
        } catch (MalformedURLException e) {
            throw new ConfigException.BadValue(path, NOT_VALID_URL_ERROR_MESSAGE, e);
        }
    }

    public static int getPageSize(Config config) {
        return config.getInt("page.size");
    }


}
