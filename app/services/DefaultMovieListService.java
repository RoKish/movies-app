package services;

import dao.DataAccessException;
import dao.DataAccessExceptionType;
import dao.MovieListDao;
import models.Movie;
import models.MovieList;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class DefaultMovieListService implements MovieListService {

    private final MovieListDao movieListDao;
    private final MovieService movieService;

    @Inject
    public DefaultMovieListService(MovieListDao movieListDao, MovieService movieService) {
        this.movieListDao = movieListDao;
        this.movieService = movieService;
    }

    public CompletionStage<MovieList> create(MovieList movieList) {
        return requireExistingMovies(movieList.getMovies())
                .thenCompose(ignored -> movieListDao.save(movieList));
    }


    @Override
    public CompletionStage<MovieList> find(int id) {
        return movieListDao.get(id)
                .thenApply(movieList -> {
                    if (movieList == null) {
                        throw new DataAccessException(DataAccessExceptionType.NOT_FOUND, "The movie list doesn't exist: " + id);
                    }
                    return movieList;
                });
    }

    @Override
    public CompletionStage<Stream<MovieList>> findAll(int page) {
        return movieListDao.getAll(page);
    }

    @Override
    public CompletionStage<MovieList> addMovies(int listId, Set<Integer> movies) {
        return requireExistingMovies(movies)
                .thenCompose(ignored -> movieListDao.saveMovies(listId, movies));
    }

    private CompletionStage<Set<Integer>> requireExistingMovies(Set<Integer> ids) {
        return movieService.find(ids)
                .thenApply(movies -> movies.map(Movie::getId).collect(Collectors.toSet()))
                .thenApply(existingMovies -> {
                    Set<Integer> nonExistingMovies = new HashSet<>(ids);
                    nonExistingMovies.removeAll(existingMovies);
                    if (!nonExistingMovies.isEmpty()) {
                        throw new RuntimeException("Unknown movies: " + nonExistingMovies);
                    }
                    return ids;
                });
    }

    @Override
    public CompletionStage<MovieList> delete(int id) {
        return movieListDao.delete(id);
    }

    @Override
    public CompletionStage<MovieList> deleteMovie(int listId, int movieId) {
        return movieListDao.deleteMovie(listId, movieId);
    }

}
