package services;

import com.google.inject.ImplementedBy;
import models.MovieList;

import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@ImplementedBy(DefaultMovieListService.class)
public interface MovieListService {

    CompletionStage<MovieList> create(MovieList movieList);

    CompletionStage<MovieList> find(int id);

    CompletionStage<Stream<MovieList>> findAll(int page);

    CompletionStage<MovieList> addMovies(int listId, Set<Integer> movies);

    CompletionStage<MovieList> delete(int id);

    CompletionStage<MovieList> deleteMovie(int listId, int movieId);

}
