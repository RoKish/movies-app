package services.pagination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class PaginationAdapter {

    private final int sourcePageSize;
    private final int targetPageSize;

    public PaginationAdapter(int sourcePageSize, int targetPageSize) {
        this.sourcePageSize = sourcePageSize;
        this.targetPageSize = targetPageSize;
    }

    public List<Page> getPages(int targetPage) {
        int targetOffset = (targetPage - 1) * targetPageSize;
        int targetLimit = targetPageSize;
        List<Page> pages = new ArrayList<>();
        while (targetLimit > 0) {
            int pageNumber = targetOffset / sourcePageSize + 1;
            int pageOffset = targetOffset % sourcePageSize;
            int pageLimit = sourcePageSize - pageOffset;
            if (pageLimit > targetLimit) {
                pageLimit = targetLimit;
            }

            targetOffset += pageLimit;
            targetLimit -= pageLimit;

            pages.add(new Page(pageNumber, pageOffset, pageLimit));
        }

        return Collections.unmodifiableList(pages);
    }
}
