package services.pagination;

import java.util.Objects;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class Page {

    private final int number;

    private final int offset;

    private final int limit;

    public Page(int number, int offset, int limit) {
        this.number = number;
        this.offset = offset;
        this.limit = limit;
    }

    public int getNumber() {
        return number;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    @Override
    public String toString() {
        return "Page{" +
                "number=" + number +
                ", offset=" + offset +
                ", limit=" + limit +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return number == page.number &&
                offset == page.offset &&
                limit == page.limit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, offset, limit);
    }
}
