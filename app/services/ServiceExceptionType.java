package services;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public enum  ServiceExceptionType {

    NOT_FOUND,
    INTERNAL

}
