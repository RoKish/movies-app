package services;

import com.google.inject.ImplementedBy;
import models.Movie;

import java.util.Collection;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@ImplementedBy(TheMovieDbMovieService.class)
public interface MovieService {

    CompletionStage<Stream<Movie>> search(String query, int page);

    CompletionStage<Movie> find(int id);

    CompletionStage<Stream<Movie>> find(Collection<Integer> ids);

}
