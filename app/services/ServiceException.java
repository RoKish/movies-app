package services;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class ServiceException extends RuntimeException {

    private final ServiceExceptionType type;

    public ServiceException(ServiceExceptionType type, String message) {
        super(message);
        this.type = type;
    }

    public ServiceException(ServiceExceptionType type, String message, Throwable cause) {
        super(message, cause);
        this.type = type;
    }
}
