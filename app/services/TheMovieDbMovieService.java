package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.typesafe.config.Config;
import models.Movie;
import play.libs.Json;
import play.libs.concurrent.Futures;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import services.pagination.Page;
import services.pagination.PaginationAdapter;
import util.ConfigUtil;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class TheMovieDbMovieService implements MovieService {

    private static final String SEARCH_RESOURCE_PATH = "/3/search/movie";
    private static final String MOVIE_RESOURCE_PATH = "/3/movie/";
    private static final int MOVIE_DB_PAGE_SIZE = 20;
    private static final String API_KEY_PARAM = "api_key";

    private final WSClient wsClient;
    private final HttpExecutionContext executionContext;
    private final Futures futures; //TODO timeout?

    private final String apiKey;
    private final String searchResourceUrl;
    private final String movieResourceUrl;
    private final PaginationAdapter paginationAdapter;

    @Inject
    public TheMovieDbMovieService(WSClient wsClient, HttpExecutionContext executionContext, Config config, Futures futures) {
        this.wsClient = wsClient;
        this.executionContext = executionContext;
        this.futures = futures;
        this.apiKey = config.getString("themoviedb.api.key");
        String movieDbUrl = ConfigUtil.getNormalizedUrl(config, "themoviedb.url").toString();
        this.searchResourceUrl = movieDbUrl + SEARCH_RESOURCE_PATH;
        this.movieResourceUrl = movieDbUrl + MOVIE_RESOURCE_PATH;
        this.paginationAdapter = new PaginationAdapter(MOVIE_DB_PAGE_SIZE, ConfigUtil.getPageSize(config));
    }

    @Override
    public CompletionStage<Stream<Movie>> search(String query, int targetPage) {
        List<Page> pages = paginationAdapter.getPages(targetPage);
        Page firstPage = pages.get(0);
        return requestPage(query, firstPage)
                .thenCompose(jsonNode -> {
                    int totalPages = jsonNode.get("total_pages").asInt();
                    CompletionStage<Stream<Movie>> movies = CompletableFuture.completedFuture(getMovies(jsonNode, firstPage));
                    for (int i = 1; i < pages.size() && i < totalPages; i++) {
                        Page page = pages.get(i);
                        movies = requestPage(query, page)
                                .thenApply(json -> getMovies(json, page))
                                .thenCombine(movies, Stream::concat);
                    }
                    return movies;
                });
    }

    @Override
    public CompletionStage<Movie> find(int id) {
        return wsClient.url(movieResourceUrl + id)
                .addQueryParameter(API_KEY_PARAM, apiKey)
                .get()
                .thenApplyAsync(response -> {
                    if (isSuccess(response)) {
                        return Json.fromJson(response.asJson(), Movie.class);
                    } else if (response.getStatus() == 404) {
                        throw new ServiceException(ServiceExceptionType.NOT_FOUND, "The movie with id '" + id + "' doesn't exist");
                    } else {
                        throw new ServiceException(ServiceExceptionType.INTERNAL, response.asJson().get("status_message").asText());
                    }
                }, executionContext.current());
    }

    private boolean isSuccess(WSResponse response) {
        return response.getStatus() - 200 < 100;
    }

    @Override
    public CompletionStage<Stream<Movie>> find(Collection<Integer> ids) {
        return ids.stream()
                .map(this::find)
                .map(stage -> stage.thenApply(Stream::of))
                .reduce((left, right) -> left.thenCombine(right, Stream::concat))
                .orElse(CompletableFuture.completedFuture(Stream.empty()));
    }

    private CompletionStage<JsonNode> requestPage(String title, Page page) {
        return wsClient.url(searchResourceUrl)
                .addQueryParameter(API_KEY_PARAM, apiKey)
                .addQueryParameter("query", title)
                .addQueryParameter("page", String.valueOf(page.getNumber()))
                .get()
                .thenApplyAsync(WSResponse::asJson, executionContext.current());
    }

    private Stream<Movie> getMovies(JsonNode json, Page page) {
        ArrayNode moviesArray = (ArrayNode) json.path("results");
        Spliterator<JsonNode> spliterator = Spliterators.spliterator(moviesArray.elements(), moviesArray.size(), Spliterator.ORDERED);
        return StreamSupport.stream(spliterator, false)
                .skip(page.getOffset())
                .limit(page.getLimit())
                .map(jsonNode -> Json.fromJson(jsonNode, Movie.class));
    }


}
