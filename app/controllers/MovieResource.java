package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.swagger.annotations.Api;
import models.Movie;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import services.MovieService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@Api
public class MovieResource extends Controller{

    private final MovieService movieService;

    @Inject
    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }

    public CompletionStage<Result> search(String query, int page) {
        return movieService.search(query, page)
                .thenApply(this::toJson)
                .thenApply(Results::ok);
    }

    public CompletionStage<Result> get(int id) {
        return movieService.find(id)
                .thenApply(this::toJson)
                .thenApply(Results::ok);
    }

    private JsonNode toJson(Stream<Movie> movies) {
        ArrayNode moviesJsonArray = Json.newArray();
        movies.map(Json::toJson).forEach(moviesJsonArray::add);
        return Json.newObject().set("movies", moviesJsonArray);
    }

    private JsonNode toJson(Movie movie) {
        return Json.toJson(movie);
    }
}
