package controllers.error;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class Error {

    private String message;

    private String detailedMessage;

    public Error(String message) {
        this.message = message;
    }

    public Error(String message, String detailedMessage) {
        this.message = message;
        this.detailedMessage = detailedMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }
}
