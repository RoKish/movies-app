package controllers.error;

import com.typesafe.config.Config;
import dao.DataAccessException;
import dao.DataAccessExceptionType;
import play.Environment;
import play.api.OptionalSourceMapper;
import play.api.UsefulException;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
@Singleton
public class ErrorHandler extends DefaultHttpErrorHandler {

    private static final Map<Class<? extends Throwable>, Function<Throwable, CompletionStage<Result>>> EXCEPTION_HANDLERS = getExceptionHandlers();

    @Inject
    public ErrorHandler(Config config, Environment environment, OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(config, environment, sourceMapper, routes);
    }

    @Override
    protected CompletionStage<Result> onProdServerError(Http.RequestHeader request, UsefulException exception) {
        Throwable originException;
        if (exception.getCause() instanceof CompletionException) {
            CompletionException completionException = (CompletionException) exception.getCause();
            originException = completionException.getCause();
        } else {
            originException = exception.getCause();
        }

        return Optional.ofNullable(EXCEPTION_HANDLERS.get(originException.getClass()))
                .map(exceptionHandler -> exceptionHandler.apply(originException))
                .orElseGet(() -> onUnknownProdServerError(request, exception.getMessage()));
    }

    private CompletionStage<Result> onUnknownProdServerError(Http.RequestHeader request, String message) {
        Error error = new Error("Internal server error", message);
        Errors errors = new Errors(error);
        return CompletableFuture.completedFuture(Results.internalServerError(Json.toJson(errors)));
    }

    @Override
    protected CompletionStage<Result> onNotFound(Http.RequestHeader request, String message) {
        Error error = new Error("Resource not found. Please check the URL", "The resource hasn't been found for the request: " +
                request.method() + " " + request.path());
        Errors errors = new Errors(error);
        return CompletableFuture.completedFuture(Results.notFound(Json.toJson(errors)));
    }


    private static Map<Class<? extends Throwable>, Function<Throwable, CompletionStage<Result>>> getExceptionHandlers() {
        Map<Class<? extends Throwable>, Function<Throwable, CompletionStage<Result>>> exceptionHandlers = new HashMap<>();
        exceptionHandlers.put(DataAccessException.class, throwable -> {
            DataAccessException exception = (DataAccessException) throwable;
            CompletionStage<Result> result = null;
            if (exception.getType() == DataAccessExceptionType.NOT_FOUND) {
                Error error = new Error(exception.getMessage());
                Errors errors = new Errors(error);
                result = CompletableFuture.completedFuture(Results.notFound(Json.toJson(errors)));
            } else if (exception.getType() == DataAccessExceptionType.ALREADY_EXISTS) {
                Error error = new Error(exception.getMessage());
                Errors errors = new Errors(error);
                result = CompletableFuture.completedFuture(Results.badRequest(Json.toJson(errors)));
            } else {
                Error error = new Error(exception.getMessage());
                if (exception.getCause() != null) {
                    error.setDetailedMessage(exception.getCause().getMessage());
                }
                Errors errors = new Errors(error);
                result = CompletableFuture.completedFuture(Results.internalServerError(Json.toJson(errors)));
            }
            return result;
        });
        return Collections.unmodifiableMap(exceptionHandlers);
    }

}
