package controllers.error;

import java.util.Collections;
import java.util.List;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class Errors {

    private List<Error> errors;

    public Errors(Error error) {
        this.errors = Collections.singletonList(error);
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
