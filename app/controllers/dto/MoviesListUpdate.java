package controllers.dto;

import java.util.Set;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
public class MoviesListUpdate {

    private Set<Integer> movies;

    public Set<Integer> getMovies() {
        return movies;
    }

    public void setMovies(Set<Integer> movies) {
        this.movies = movies;
    }
}
