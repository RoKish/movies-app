package controllers.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import models.Movie;

import java.util.List;

/**
 * @author Roman Kishchenko
 * @since 8/22/17
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MovieList {

    private int id;

    private String name;

    private List<Movie> movies;

    @JsonProperty
    public int getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}