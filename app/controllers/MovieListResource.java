package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.typesafe.config.Config;
import controllers.dto.MovieList;
import controllers.dto.MoviesListUpdate;
import io.swagger.annotations.Api;
import models.Movie;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import services.MovieListService;
import services.MovieService;
import util.ConfigUtil;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
@Api
public class MovieListResource extends Controller {

    private final MovieListService movieListService;
    private final MovieService movieService;
    private final String baseUrl;

    @Inject
    public MovieListResource(MovieListService movieListService, MovieService movieService, Config config) {
        this.movieListService = movieListService;
        this.movieService = movieService;
        this.baseUrl = ConfigUtil.getNormalizedUrl(config, "base.url").toString();
    }


    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> create() {
        JsonNode json = request().body().asJson();
        models.MovieList movieList = Json.fromJson(json, models.MovieList.class);
        movieList.setActive(true);
        return movieListService.create(movieList)
                .thenApply(this::buildResourceUrl)
                .thenApply(url -> created().withHeader(LOCATION, url));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> addMovies(int id) {
        JsonNode movies = request().body().asJson();
        MoviesListUpdate moviesListUpdate = Json.fromJson(movies, MoviesListUpdate.class);
        return movieListService.addMovies(id, moviesListUpdate.getMovies())
                .thenApply(movieList -> convert(movieList, true))
                .thenApply(Json::toJson)
                .thenApply(Results::ok);
    }

    private String buildResourceUrl(models.MovieList movieList) {
        StringBuilder resourceUrl = new StringBuilder(baseUrl);
        String path = request().path();
        if (!path.startsWith("/")) {
            resourceUrl.append('/');
        }
        resourceUrl.append(path);
        if (!path.endsWith("/")) {
            resourceUrl.append('/');
        }
        resourceUrl.append(movieList.getId());
        return resourceUrl.toString();
    }

    public CompletionStage<Result> get(int id) {
        return movieListService.find(id)
                .thenCompose(this::fetchMoviesAndConvert)
                .thenApply(Json::toJson)
                .thenApply(Results::ok);
    }

    public CompletionStage<Result> getAll(int page) {
        return movieListService.findAll(page)
                .thenCompose(this::fetchMoviesAndConvert)
                .thenApply(this::toJson)
                .thenApply(Results::ok);
    }

    private CompletionStage<Stream<MovieList>> fetchMoviesAndConvert(Stream<models.MovieList> movieLists) {
       return movieLists.map(this::fetchMoviesAndConvert)
                .map(stage -> stage.thenApply(Stream::of))
                .reduce((left, right) -> left.thenCombine(right, Stream::concat))
               .orElse(CompletableFuture.completedFuture(Stream.empty()));
    }

    private CompletionStage<MovieList> fetchMoviesAndConvert(models.MovieList movieList) {
        return movieService.find(movieList.getMovies())
                .thenApply(movies -> {
                    MovieList result = convert(movieList, false);
                    result.setMovies(movies.collect(Collectors.toList()));
                    return result;
                });
    }

    public CompletionStage<Result> delete(int id) {
        return movieListService.delete(id)
                .thenApply(movieList -> noContent());
    }

    public CompletionStage<Result> deleteMovie(int listId, int movieId) {
        return movieListService.deleteMovie(listId, movieId)
                .thenApply(movieList -> convert(movieList, true))
                .thenApply(Json::toJson)
                .thenApply(Results::ok);
    }

    private MovieList convert(models.MovieList movieList, boolean convertMovies) {
        MovieList result = new MovieList();
        result.setName(movieList.getName());
        result.setId(movieList.getId());
        if (convertMovies) {
            result.setMovies(movieList.getMovies()
                    .stream()
                    .map(movieId -> {
                        Movie movie = new Movie();
                        movie.setId(movieId);
                        return movie;
                    })
                    .collect(Collectors.toList()));
        }

        return result;
    }

    private JsonNode toJson(Stream<MovieList> movieLists) {
        ArrayNode listsArray = Json.newArray();
        movieLists.map(Json::toJson).forEach(listsArray::add);
        return Json.newObject().set("lists", listsArray);
    }
}
