package util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.URL;

import static org.testng.Assert.*;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class ConfigUtilTest {

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {"http://example.com:80/", "http://example.com"},
                {"http://example.com/", "http://example.com"},
                {"http://example.com:80", "http://example.com"},
                {"http://example.com:8080/", "http://example.com:8080"},
                {"http://example.com:8080", "http://example.com:8080"},
                {"http://example.com:80/resource/", "http://example.com/resource"},
                {"HTTP://example.com", "http://example.com"},
        };
    }

    @Test(dataProvider = "data")
    public void testGetNormalizedUrl(String source, String expected) throws Exception {
        Config config = config(source);
        URL url = ConfigUtil.getNormalizedUrl(config, "url");
        Assert.assertEquals(url.toString(), expected);
    }

    @Test(expectedExceptions = ConfigException.BadValue.class)
    public void testGetNormalizedUrlException() {
        Config config = config("invalidUrk");
        ConfigUtil.getNormalizedUrl(config, "url");
    }

    private Config config(String sourceUrl) {
        Config config = Mockito.mock(Config.class);
        Mockito.when(config.getString(Mockito.anyString()))
                .thenReturn(sourceUrl);
        return config;
    }

}