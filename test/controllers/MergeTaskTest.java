//package controllers;
//
//import org.testng.Assert;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//import java.util.Arrays;
//
///**
// * @author Roman Kishchenko
// * @since 8/18/17
// */
//public class MergeTaskTest {
//
//    @DataProvider
//    public Object[][] testData() {
//        return new Object[][]{
//                {array(1, 7, 9, 15, 19, 21, 23), array(2, 4, 22), array(1, 2, 4, 7, 9, 15, 19, 21, 22, 23)},
//                {array(1, 2, 3), array(4, 5, 6), array(1, 2, 3, 4, 5, 6)},
//                {array(1, 2, 3), array(), array(1, 2, 3)},
//                {array(), array(1, 2, 3), array(1, 2, 3)},
//        };
//    }
//
//    @Test(dataProvider = "testData")
//    public void test(int[] left, int[] right, int[] expected) {
//        MergeTask mergeTask = new MergeTask(left, right);
//        int[] actual = mergeTask.compute();
//        Assert.assertTrue(Arrays.equals(actual, expected));
//    }
//
//    private int[] array(int... elements) {
//        return elements;
//    }
//
//}