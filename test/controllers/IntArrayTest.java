package controllers;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Roman Kishchenko
 * @since 8/18/17
 */
public class IntArrayTest {

    @Test
    public void testToArray() {
        int[] source = new int[]{1, 2, 3};
        IntArray array = IntArray.of(source);
        Assert.assertEquals(array.toArray(), source);
    }

    @Test
    public void testSubArrayFromIndex() {
        IntArray array = IntArray.of(1, 2, 3);
        IntArray subArray = array.subArray(1);
        Assert.assertEquals(subArray.toArray(), new int[]{2, 3});
    }

    @Test
    public void testSubArrayFromToIndex() {
        IntArray array = IntArray.of(1, 2, 3);
        IntArray subArray = array.subArray(1, 2);
        Assert.assertEquals(subArray.toArray(), new int[]{2});
    }

    @Test
    public void testSubArraySubArray() {
        IntArray array = IntArray.of(1, 2, 3);
        IntArray subArray = array.subArray(1, 3);
        IntArray subArraySubArray = subArray.subArray(1, 2);
        Assert.assertEquals(subArraySubArray.toArray(), new int[]{3});
    }

    @Test
    public void testArrayGet() {
        IntArray array = IntArray.of(1, 2, 3);
        Assert.assertEquals(array.get(0), 1);
        Assert.assertEquals(array.get(1), 2);
        Assert.assertEquals(array.get(2), 3);
    }


}