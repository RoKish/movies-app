package services.pagination;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * @author Roman Kishchenko
 * @since 8/21/17
 */
public class PaginationAdapterTest {

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {20, 1, 10, singletonList(new Page(1, 0, 10))},
                {20, 2, 25, asList(new Page(2, 5, 15), new Page(3, 0, 10))},
                {10, 2, 30, asList(new Page(4, 0, 10), new Page(5, 0, 10),new Page(6, 0, 10))},
                {20, 5, 5, singletonList(new Page(2, 0, 5))},
                {20, 6, 5, singletonList(new Page(2, 5, 5))}
        };
    }

    @Test(dataProvider = "data")
    public void test(int sourcePageSize, int targetPage, int targetPageSize, List<Page> expected) {
        PaginationAdapter paginationAdapter = new PaginationAdapter(sourcePageSize, targetPageSize);
        List<Page> pages = paginationAdapter.getPages(targetPage);
        Assert.assertEquals(pages, expected);
    }

}